﻿using UnityEngine;

public class Mover : MonoBehaviour 
{
    private Rigidbody _rigidbody;

    [SerializeField] private float speed;
    
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();

        _rigidbody.velocity = transform.forward * speed;
    }
}

﻿using UnityEngine;

[System.Serializable]
public struct Boundary
{
    public float XMin, xMax;
    public float zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Boundary _boundary;
    [SerializeField] private float _tilt;

    public GameObject shot;
    public Transform shotSpawn;
    [SerializeField] private float fireRate;
    private float nextFire;
    
    private Rigidbody _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moverVertcal = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moverVertcal);

        _rigidbody.velocity = movement * speed;
        
        _rigidbody.position = new Vector3
            (
                Mathf.Clamp(_rigidbody.position.x, _boundary.XMin, _boundary.xMax), 
                0.0f, 
                Mathf.Clamp(_rigidbody.position.z, _boundary.zMin, _boundary.zMax)
            );
        
        _rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, _rigidbody.velocity.x * -_tilt);
        
    }
}

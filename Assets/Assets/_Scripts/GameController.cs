﻿using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject hazard;
    [SerializeField] private Vector3 _spawnValues;
    [SerializeField] private int hazardCount;
    [SerializeField] private float spanWait;
    [SerializeField] private float startWait;
    [SerializeField] private float waveDelay;

    private void Start()
    {
        StartCoroutine(SpawnWaves());
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-_spawnValues.x, _spawnValues.x),
                    _spawnValues.y, _spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spanWait);
            }
            yield return new WaitForSeconds(waveDelay);
        }
    }
}
